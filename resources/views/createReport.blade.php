<?php 
/**
 * Bootstrap CDN tempalte by Kolorbot
 * https://gist.github.com/kolorobot/1cea9f09d6ff4f7e8096
 * 
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>

    <title>reportToPDF! :: Do by my OWN</title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css"/>

    <!-- Custom CSS -->
    <style>
        body {
            padding-top: 50px;
        }

        .starter-template {
            padding: 40px 15px;
            text-align: center;
        }
        html, body {
            padding-top: 20px;
        }

        [data-role="dynamic-fields"] > .form-inline + .form-inline {
            margin-top: 0.5em;
        }

        [data-role="dynamic-fields"] > .form-inline [data-role="add"] {
            display: none;
        }

        [data-role="dynamic-fields"] > .form-inline:last-child [data-role="add"] {
            display: inline-block;
        }

        [data-role="dynamic-fields"] > .form-inline:last-child [data-role="remove"] {
            display: none;
        }
        .btn-black{
            background: black;
            border:unset;
        }
        .btn-black:hover{
            background: white;
            border: 1px black solid;
            color: black;
        }
    </style>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!--<a class="navbar-brand" href="#">Project name</a>-->
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="/{{$data['url']}}">Do my own</a></li>
                <li><a href="/{{$data['url']}}/generatefromcsv" >From CSV</a></li>
<!--                 <li><a href="#contact">Contact</a></li> -->
            </ul>
        </div>
        <!--/.nav-collapse -->
    </div>
    <!-- /.container -->
</nav>

<!-- Page Content -->
<div class="container">
    <div class="row">
        <div class="starter-template">
            <h1>reportToPDF</h1>
            <p>Easy tool to make and print your report, in PDF.</p>

           <!--  <p class="lead">Complete with pre-defined CDN paths!</p>

            <p><a href="http://getbootstrap.com/getting-started/">http://getbootstrap.com/getting-started/</a></p> -->
        </div>

                    <form class="form-horizontal" method="post" action="/rtp" name="report">
            <fieldset>
            @csrf

            <!-- Text input-->
            <div class="form-group">
            <label class="col-md-4 control-label" for="textinput">From</label>  
            <div class="col-md-4">
            <input id="textinput" name="from" type="text" required placeholder="IT departament" class="form-control input-md">
            <span class="help-block">help</span>  
            </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
            <label class="col-md-4 control-label" for="textinput">To</label>  
            <div class="col-md-4">
            <input id="name" name="to" type="text" required placeholder="Financial services" class="form-control input-md">
            <span class="help-block">help</span>  
            </div>
            </div>

            <!--title-->

                     <div class="form-group">
            <label class="col-md-4 control-label" for="textinput">Title</label>  
            <div class="col-md-4">
            <input id="name" name="title" type="text" required placeholder="Amazon pursaches on may" class="form-control input-md">
            <span class="help-block">help</span>  
            </div>
            </div>

            

            <!-- Prefix(number) text-->
            <div class="form-group">
            <label class="col-md-4 control-label" for="prependedtext">Number</label>
            <div class="col-md-4">
                <div class="input-group">
                <span class="input-group-addon">{{$data['prefix']}}</span>
                <input id="prependedtext" required name="number" class="form-control" placeholder="5543" type="number">
                </div>
                <p class="help-block">help</p>
            </div>
            </div>

            <!--b-->

            <div class="form-group">
            <label class="col-md-4 control-label" for="prependedtext">Concepts:</label>

            <div class="col-md-4">
            <div data-role="dynamic-fields">
                <div class="form-inline">
                    <span>-</span>
                    <div class="form-group">
                        <input type="text"  name="concept[]" class="form-control" id="field-value" required="required" placeholder="fee">
                    </div>
                    <button class="btn btn-danger" data-role="remove">
                        <span class="glyphicon glyphicon-remove"></span>
                    </button>
                    <button class="btn-black btn btn-primary" data-role="add">
                        <span class="glyphicon glyphicon-plus"></span>
                    </button>
                </div>  <!-- /div.form-inline -->
            </div>  <!-- /div[data-role="dynamic-fields"] -->
        </div>  <!-- /div.col-md-12 -->
            </div>



            <!-- Textarea -->
            <div class="form-group">
            <label class="col-md-4 control-label" for="textarea">Observations</label>
            <div class="col-md-4">                     
                <textarea class="form-control"  id="textarea" name="observations"></textarea>
            </div>
            </div>

            <!-- Button -->
            <div class="form-group">
            <label class="col-md-4 control-label" for="singlebutton"></label>
            <div class="col-md-4">
                <button id="singlebutton" class="btn-black btn btn-primary">Print it!</button>
            </div>
            </div>

            </fieldset>
            </form>

    </div>
    <!-- /.row -->
</div>
<!-- /.container -->
<!-- Footer -->
<footer class="page-footer font-small" style='padding-top:4em;'>

  <!-- Copyright -->
  <div  style="background:black; color:white;" class="footer-copyright text-center py-3">by Gassó | 
    <a style="color:white;" href="https://gitlab.com/angelixo/reporttopdf">reportToPDF</a>
  </div>
  <!-- Copyright -->

</footer>
<script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
<script>
$(function() {
    // Remove button click
    $(document).on(
        'click',
        '[data-role="dynamic-fields"] > .form-inline [data-role="remove"]',
        function(e) {
            e.preventDefault();
            $(this).closest('.form-inline').remove();
        }
    );
    // Add button click
    $(document).on(
        'click',
        '[data-role="dynamic-fields"] > .form-inline [data-role="add"]',
        function(e) {
            e.preventDefault();
            var container = $(this).closest('[data-role="dynamic-fields"]');
            new_field_group = container.children().filter('.form-inline:first-child').clone();
            new_field_group.find('input').each(function(){
                $(this).val('');
            });
            container.append(new_field_group);
        }
    );
});
</script>
</body>
</html>