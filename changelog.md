# Changelog

All notable changes to `reportToPdf` will be documented in this file.
## Verion 2.0.0

-Support to import CSV files 

## Version 1.1.0

-Fix DOM PDF issue. 

## Version 1.0.0

- Complete form based on bootstrap
- Prints a pdf based on a html template
- Set conf files (do not forget to vendor:publish!!)

## Version 0.0.2

- First route and packalist link. 

## Version 0.0.1

- Everything
