# reportToPdf

[![Latest Version on Packagist][ico-version]][link-packagist]
[![Total Downloads][ico-downloads]][link-downloads]
[![Build Status][ico-travis]][link-travis]

An easy tool to make your own report and print it! Its allow to import CSV to make the concepts. The template of report can be easy customized from config folder and is based on HTML and CSS tables.

**Working In Progress**

## Installation

Via Composer

``` bash
$ composer require gasso/reporttopdf
```
Link storage if is needed
```bash 
$ php artisan storage:link
```
and publish the configuration file to your config folder
```bash 
$ php artisan vendor:publish
````


## Usage

You just need set your url on `reporttopdf.php` file on your config folder and navigate it.

## Change log

Please see the [changelog](changelog.md) for more information on what has changed recently.

## Testing (Working in progress)

``` bash
$ composer test
```

## Contributing

Please see [contributing.md](contributing.md) for details and a todolist.

## Security

If you discover any security related issues, please email angelixo@gmail.com instead of using the issue tracker.

## Credits

- [angel][link-author]
- [All Contributors][link-contributors]

## License

MIT. Please see the [license file](license.md) for more information.

[ico-version]: https://img.shields.io/packagist/v/gasso/reporttopdf.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/gasso/reporttopdf.svg?style=flat-square
[ico-travis]: https://img.shields.io/travis/gasso/reporttopdf/master.svg?style=flat-square
[ico-styleci]: https://styleci.io/repos/12345678/shield

[link-packagist]: https://packagist.org/packages/gasso/reporttopdf
[link-downloads]: https://packagist.org/packages/gasso/reporttopdf
[link-travis]: https://travis-ci.org/gasso/reporttopdf
[link-styleci]: https://styleci.io/repos/12345678
[link-author]: https://github.com/gasso
[link-contributors]: ../../contributors
