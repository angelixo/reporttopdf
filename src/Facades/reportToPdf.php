<?php

namespace gasso\reportToPdf\Facades;

use Illuminate\Support\Facades\Facade;

class reportToPdf extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'reporttopdf';
    }
}
