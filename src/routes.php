<?php

/**
 * Routes 
 */
use Illuminate\Http\Request;
use Dompdf\Dompdf;

Route::get(config('reporttopdf.data.url'), function(){
    
    $myOwnData= config('reporttopdf.data');
    $data = [
        'data'=> $myOwnData,
    ];
    
    return view('gasso::createReport',$data);
});

/**
 * Print de PDF File!
 */
Route::post('rtp', function(Request $request){

    $myOwnData= config('reporttopdf.data');
    $formData = $request;
    $formData->number= config('reporttopdf.data.prefix') . $formData->number;
    $dataForPDF = [
        'data'=> $myOwnData,
        'formData' => $formData,
    ];
    $dompdf = new Dompdf();
    $dompdf->load_html(view('gasso::reportTemplate', $dataForPDF));
    $dompdf->render();
    $dompdf->stream($formData->number . ".pdf");

});

/*Generate from CSV funcionality*/ 


Route::get(config('reporttopdf.data.url')."/generatefromcsv", function(){
    
    $myOwnData= config('reporttopdf.data');
    $dataForPDF = [
        'data'=> $myOwnData,
    ];
    return view('gasso::createFromCsv', $dataForPDF);
});

/* Generate from PDF controller*/ 
Route::post(config('reporttopdf.data.url')."/generatefromcsv", function(Request $request){

    if (!$request->file('csv')->isValid()) {
        return "Algo salio mal";
    }
    //File tasks
    $path = $request->csv->store('reportToPdfCSV');
    $data = array_map('str_getcsv', file(Storage::disk('local')->path($path)));
    Storage::delete($path); //delete file
    $headers = $data[0]; //headers for table
    unset($data[0]); //delete headers from data array
    //Pdf data
    $myOwnData= config('reporttopdf.data');
    $formData = $request->all();
    $formData['concept'] = $data;
    $formData['number']= config('reporttopdf.data.prefix') . $formData['number'];
    $dataForPDF = [
        'data' =>  $myOwnData,
        'formData' => $formData,
        'headers' => $headers
    ];

    //return $dataForPDF;
    //return view('gasso::reportFromCsv', $dataForPDF);

    $dompdf = new Dompdf();
    $dompdf->load_html(view('gasso::reportFromCsv', $dataForPDF));
    $dompdf->render();
    $dompdf->stream($formData['number'] . ".pdf");

    
 

});


