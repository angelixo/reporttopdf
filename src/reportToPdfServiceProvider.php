<?php

namespace gasso\reportToPdf;

use Illuminate\Support\ServiceProvider;

class reportToPdfServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'gasso');
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'gasso');
        // $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadRoutesFrom(__DIR__.'/routes.php');

        // Publishing is only necessary when using the CLI.
        if ($this->app->runningInConsole()) {
            $this->bootForConsole();
        }
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/reporttopdf.php', 'reporttopdf');

        // Register the service the package provides.
        $this->app->singleton('reporttopdf', function ($app) {
            return new reportToPdf;
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['reporttopdf'];
    }
    
    /**
     * Console-specific booting.
     *
     * @return void
     */
    protected function bootForConsole()
    {
        // Publishing the configuration file.
        $this->publishes([
            __DIR__.'/../config/reporttopdf.php' => config_path('reporttopdf.php'),
        ], 'reporttopdf.config');

        // Publishing the views.
        /*$this->publishes([
            __DIR__.'/../resources/views' => base_path('resources/views/vendor/gasso'),
        ], 'reporttopdf.views');*/

        // Publishing assets.
        /*$this->publishes([
            __DIR__.'/../resources/assets' => public_path('vendor/gasso'),
        ], 'reporttopdf.views');*/

        // Publishing the translation files.
        /*$this->publishes([
            __DIR__.'/../resources/lang' => resource_path('lang/vendor/gasso'),
        ], 'reporttopdf.views');*/

        // Registering package commands.
        // $this->commands([]);
    }
}
